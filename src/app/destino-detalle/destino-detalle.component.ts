import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { DestinoViaje } from '../destino-viaje.model';
import { DestinosApiClient } from '../destinos-api-client.model';

@Component({
  selector: 'app-destino-detalle',
  templateUrl: './destino-detalle.component.html',
  styleUrls: ['./destino-detalle.component.css']
})
export class DestinoDetalleComponent implements OnInit {

  destino : DestinoViaje

  constructor( private route : ActivatedRoute,
    private destinosApiClient : DestinosApiClient ) { }

  ngOnInit(): void {
    let id = this.route.snapshot.paramMap.get('id')
    this.destino = null
  }

}
