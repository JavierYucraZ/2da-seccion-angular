import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, ValidatorFn, Validators } from '@angular/forms';
import { fromEvent } from 'rxjs';
import { DestinoViaje } from '../destino-viaje.model';
import { map, filter, distinctUntilChanged, switchMap, debounceTime } from 'rxjs/operators'
import { ajax } from 'rxjs/ajax'

@Component({
  selector: 'app-form-destino-viaje',
  templateUrl: './form-destino-viaje.component.html',
  styleUrls: ['./form-destino-viaje.component.css']
})
export class FormDestinoViajeComponent implements OnInit {

  @Output() onItemAdded : EventEmitter<DestinoViaje>
  fg : FormGroup
  minLongitud = 5
  SearchResult : string[]

  constructor(private fb : FormBuilder) { 
    this.onItemAdded = new EventEmitter()
    this.fg = this.fb.group({
      nombre : ['', Validators.compose([
        Validators.required,
        this.nombreValidatorParametrizable(this.minLongitud)
      ])],
      url : ['', Validators.required]
    })


    // this.fg.valueChanges.subscribe( ( form ) => {
    //   console.log(`Cambio el formulario : `, form)
    // })
  }


  ngOnInit(){
    let elemNombre = <HTMLInputElement>document.getElementById('nombre')
    fromEvent(elemNombre, 'input')
    .pipe(
      map((e : KeyboardEvent) => ( e.target as HTMLInputElement).value),
      filter(text => text.length > 4),
      debounceTime(200),
      distinctUntilChanged(),
      switchMap(() => ajax('/assets/datos.json'))
    ).subscribe( ajaxResponse => {
      this.SearchResult = ajaxResponse.response
    })
  }

  guardar(nombre : string, url : string){
    const destino = new DestinoViaje(nombre, url)
    this.onItemAdded.emit(destino)
    
  }


  nombreValidator(control : FormControl) : { [key : string] : boolean} {
    const longitud = control.value.toString().trim().length
    if(longitud > 0 && longitud < 5){
      return { minLongNombre : true } 
    }
    return null
  }

  nombreValidatorParametrizable(minLong : number) : ValidatorFn{
    return (control : FormControl) : {[key : string] : boolean} | null => {
      const longitud = control.value.toString().trim().length
      if(longitud > 0 && longitud < minLong){
        return { minLongNombre : true } 
      }
      return null
    }
  }

}
