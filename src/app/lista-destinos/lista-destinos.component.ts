import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { Store } from '@ngrx/store';
import { AppState } from '../app.module';
import { DestinoViaje } from '../destino-viaje.model';
import { DestinosApiClient } from '../destinos-api-client.model';

@Component({
  selector: 'app-lista-destinos',
  templateUrl: './lista-destinos.component.html',
  styleUrls: ['./lista-destinos.component.css'],
})
export class ListaDestinosComponent implements OnInit {
  @Output() onItemAdded: EventEmitter<DestinoViaje>;
  updates: string[] = []
  all

  constructor(
    public destinosApiClient: DestinosApiClient,
    private store: Store<AppState>
  ) {
    this.onItemAdded = new EventEmitter();
    this.store
      .select((state) => state.destinos.favorito)
      .subscribe((data) => {
        if (data != null) {
          this.updates.push(`Se escogio a ${data.nombre}`);
        }
      })
    store.select(state => state.destinos.items).subscribe(items => this.all = items)
  }

  ngOnInit(): void {}

  agregado(destino: DestinoViaje) {
    this.destinosApiClient.add(destino);
    this.onItemAdded.emit(destino);
  }

  elegido(destino: DestinoViaje) {
    this.destinosApiClient.elegir(destino);
  }

}
